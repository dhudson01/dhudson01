#RCode
#spei06

## Find the trend in the spei data and plot them at each location in the NE region

rm(list=ls())


require(stats)
library(chron)
library(RColorBrewer)
library(lattice)
library(ncdf4)
library(raster)



#there are 48 timescales that you can download, this one is spei06

ncname <- "spei06"
ncfname <- paste(ncname, ".nc", sep = "")
ncin <- nc_open(ncfname)
print(ncin)

lon <- ncvar_get(ncin, "lon") #read data from the netCDF file, with the variable lon within the data
nlon <- dim(lon) #dimension of lon
head(lon)

lat <- ncvar_get(ncin, "lat", verbose = FALSE)
nlat <- dim(lat)
head(lat)

print(c(nlon,nlat))
#Get the time variable and its attributes using the ncvar_get()
#and ncatt_get() functions, 
#and also get the number of times 
#using the dim() function.

time <- ncvar_get(ncin, "time")
tunits <- ncatt_get(ncin,"time", "units")
ntime <- dim(time)

dname <- "spei"


#Get the variable and its attributes,
#and verify the size of the array.


spei.array <- ncvar_get(ncin,dname)
spei <- spei.array
dlname <- ncatt_get(ncin,dname,"long_name")
dunits <- ncatt_get(ncin,dname,"units")
fillvalue <- ncatt_get(ncin,dname,"_FillValue")
dim(spei)

#Get the global attributes

title <- ncatt_get(ncin, 0, "title")
institution <- ncatt_get(ncin, 0, "institution")
datasource <- ncatt_get(ncin, 0, "source")
references <- ncatt_get(ncin, 0, "references")
history<- ncatt_get(ncin, 0, "history")
Conventions <- ncatt_get(ncin, 0, "Conventions")
nc_close(ncin)


#convert the time variable
#split the time units string into fields
tustr <- strsplit(tunits$value, " ")
tdstr <- strsplit(unlist(tustr)[3], "-")
tmonth = as.integer(unlist(tdstr)[2])
tday = as.integer(unlist(tdstr)[3])
tyear = as.integer(unlist(tdstr)[1])
chron(time, origin = c(tmonth, tday, tyear)) #the chron() function to determine the absolute value of each time value from the time origin.

spei.array[spei.array == fillvalue$value] <- NA #replace the missing values with R NA's
length(na.omit(as.vector(spei.array[, , 1])))


#creating a data frame

lonlat <- expand.grid(lon, lat)
names(lonlat) <- c("lon","lat")

t <- as.Date(time, origin = "1900-01-01")

spei.vec.long <- as.vector(spei.array)
length(spei.vec.long)

spei.mat <- matrix(spei.vec.long, nrow = nlon * nlat, ncol = ntime)
dim(spei.mat)

head(na.omit(spei.mat))

lonlat <- expand.grid(lon, lat)
spei.data <- data.frame(cbind(lonlat, spei.mat))
names(spei.data) <- c("lon", "lat", paste(t))



###### Make a code that finds the frequency, intensity and duration for all the other other grid points

#Get Northeast grid points

sublon <- spei.data[spei.data$lon >= -80.5243 & spei.data$lon <= -66.8628,]
sublonlat <- sublon[sublon$lat >= 37.8889 & sublon$lat <= 47.455,]


## Get all the start dates and end dates for all grid points


list_of_NAs <- list()
for (q in 1:length(sublonlat)){
  if(all(is.na(sublonlat[,q])) == TRUE){
    list_of_NAs[[q]] <- q
  }
}

list_of_NAs

D_time <- t

sublonlat2 <- sublonlat[,-c(1:length(list_of_NAs))]
sublonlat3 <- na.omit(sublonlat2)
sublonlat4 <- t(sublonlat3)
D_time2 <- D_time[-c(1:(length(list_of_NAs)-2))]

NE.data <- data.frame(sublonlat4)

NElist <- list()
for (l in 1:393){
  NElist[[l]] <- 1 + which((NE.data[-1,l] <= -1) & (NE.data[-dim(NE.data)[1],l] > -1))
} 

listofones <- which(NE.data[1,] <= -1)


for (f in 1:length(listofones)){
  g <- listofones[f]
  NElist[[g]] <- append(NElist[[g]],1,after = 0)
}


NEendlist <- list()
for (m in 1:393){
  NEendlist[[m]] <- which((NE.data[-1,m] > -1) & (NE.data[-dim(NE.data)[1],m] <= -1))
}


list_4_neendlist <- which(NE.data[length(NE.data[[1]]),] <= -1)
list_4_neendlist
### if list_4_neendlist = 0 then don't use the function below
# for(n in 1:length(list_4_neendlist)){
#   h <- list_4_neendlist[n]
#   NEendlist[[h]] <- append(NEendlist[[h]],length(NE.data[[1]]),after = length(NEendlist[[h]]))
# }


# Find start years and end years

start_year <- list()
for (e in 1:393){
  start_year[[e]] <- format(as.Date(D_time[NElist[[e]]], format="%Y-%m-%d"),"%Y") 
}

end_year <- list()
for (f in 1:393){
  end_year[[f]] <- format(as.Date(D_time[NEendlist[[f]]], format="%Y-%m-%d"),"%Y") 
}


num_start_year <- list()
num_end_year <- list()
for (h in 1:393){
  num_start_year[[h]] <- as.numeric(as.character(unlist(start_year[[h]])))
  num_end_year[[h]] <- as.numeric(as.character(unlist(end_year[[h]])))
}

list_avg_year <- list()
for(t in 1:393){
  list_avg_year[[t]] <- (num_end_year[[t]]+num_start_year[[t]])/2
}

#P-value and Slope for all frequencies

n_months <- length(NE.data[[1]])
mydrought_array <- matrix(0, nrow=n_months, ncol= 393)
for(m in 1:393) {
  n_droughts <- length(NElist[[m]])
  for(drought in 1:n_droughts){
    start_month <- NElist[[m]][drought]
    end_month <- NEendlist[[m]][drought]
    duration <- end_month - start_month + 1
    mydrought_array[start_month:end_month,m]<- 1/duration
    
  }
}

#SPEI drought duration-intensity-frequency
myts <- ts(example4, start=c(1901, 1), end=c(2015, 12), frequency=12) 
plot(myts,
     main = "Time Series Frequency",
     ylab = "Frequency",
     xlab = "Time")


sublonlat7 <- sublonlat[,-c(3:length(list_of_NAs))]
sublonlat8 <- na.omit(sublonlat7)
sublonlat9 <- t(sublonlat8)
D_time2 <- D_time[-c(1:(length(list_of_NAs)-2))]

NE.data2 <- data.frame(sublonlat9)

rm(sublonlat7,sublonlat8,sublonlat9,NE.data2)

#location lon: -73.75, lat: 40.75 Frequency
location = which(NE.data2[1,] == -73.75 & NE.data2[2,] == 40.75)
example4 <- data.frame(mydrought_array[,location])

myts <- ts(example4, start=c(1901, 1), end=c(2015, 12), frequency=12) 
plot(myts,
     main = "Time Series Frequency Lon: -73.75 & Lat: 40.75",
     ylab = "Frequency",
     xlab = "Time")

#duration

GP_duration <- list()
for (o in 1:393){
  GP_duration[[o]] <- 1+ (NEendlist[[o]] - NElist[[o]])
}

Location = data.frame(NEendlist[location])
Location = array(Location)

DurationEndTime = D_time2[Location[,1]]

Duration = data.frame(GP_duration[location])
Duration = c(Duration[,1])
length(Duration)

plot(DurationEndTime, Duration,
     main = "Time Series Duration Lon: -73.75 & Lat: 40.75",
     ylab = "Duration",
     xlab = "Time")

ThisFrame = data.frame(cbind(Duration, DurationEndTime))
ThisFrame$DurationEndTime = D_time2[Location[,1]]

library(caTools)
set.seed(seed = 123)
split = sample.split(ThisFrame$Duration, SplitRatio = 0.80)
split
training_set <- subset(ThisFrame,split == TRUE)
test_set <- subset(ThisFrame,split == FALSE)

#Fitting SLR on Linear Regression to the Training set
regressor <- lm(Duration ~ DurationEndTime, data = training_set)

#Predict test results
y_pred <- predict(regressor, newdata = test_set)
library(ggplot2)
ggplot() +
  geom_point(aes(x = test_set$DurationEndTime,y = test_set$Duration), 
             colour = 'red') +
  geom_line(aes( x = training_set$DurationEndTime, y = predict(regressor, newdata = training_set)),
            colour = 'blue')
  

## Intensity

### p is length of each the NE

p <- sapply(NElist,length)

newlist2 <- matrix(nrow = 393,ncol = p[which.max(p)])
for (a in 1:393){
  for (b in 1:p[a]){
    newlist2[a,b]<- (-1 - (mean(NE.data[data.frame(NElist[[a]])[b,1]:data.frame(NEendlist[[a]])[b,1],a])))
  }
}

intensity <- t(newlist2)

Intensity <- list()
for(c in 1:393){
  Intensity[[c]] <- na.omit(intensity[,c])
}

IntensityFrame = cbind(data.frame(Intensity[[location]]),
                       data.frame(list_avg_year[[location]]))

plot(IntensityFrame$list_avg_year..location.., IntensityFrame$Intensity..location..,
     type = "l",
     main = "Time Series Intensity Lon: -73.75 & Lat: 40.75",
     ylab = "Intensity",
     xlab = "Time")


#Frequency P-val, Slope
for(r in 1:393){
  example3 <- c(1:length(NE.data[[1]]))
  
  example4 <- cbind(data.frame(mydrought_array[,r]),data.frame(example3))
  example4[example4 == 0] <- NA
  example5 <- na.omit(example4) 
  
  y <- as.vector(example5[,1])
  x <- as.vector(example5[,2])
  
  model <- mblm(y~x)
  
  Pvalue[[r]] = as.numeric(summary(model)$coefficients[2,4])
  Slope[[r]] = as.numeric(summary(model)$coefficients[2,1])
  
}

#Duration P-val, Slope
for(u in 1:393){
  
  for_duration <- cbind(data.frame(GP_duration[[u]]),data.frame(list_avg_year[[u]]))
  
  y <- as.vector(for_duration[,1])
  x <- as.vector(for_duration[,2])
  
  model1 <- mblm(y~x)
  
  Pvalue[[u]] = as.numeric(summary(model1)$coefficients[2,4])
  Slope[[u]] = as.numeric(summary(model1)$coefficients[2,1])
  
}

#Intensity P-val, Slope
for(z in 1:393){
  
  for_intensity <- cbind(data.frame(letsee[[z]]),data.frame(list_avg_year[[z]]))
  
  y <- as.vector(for_intensity[,1])
  x <- as.vector(for_intensity[,2])
  
  model2 <- mblm(y~x)
  
  Pvalue[[z]] = as.numeric(summary(model2)$coefficients[2,4])
  Slope[[z]] = as.numeric(summary(model2)$coefficients[2,1])
  
}


